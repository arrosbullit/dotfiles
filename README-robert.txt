No, al final no faig servir zsh mai!!!
Install zsh first using this thing
    apt-get install zsh
    apt-get install git-core

    Getting zsh to work in ubuntu is weird, since sh does not understand the source command. So, you do this to install zsh

    wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh

    and then you change your shell to zsh

    chsh -s `which zsh`

    and then restart

    sudo shutdown -r 0

And then use this repository by replacing the $HOME/.zshrc file created by
the above script by this one:
    #@file: .zshrc
        #source '/home/barix/dotfiles/zsh/zshrc_manager.sh'
        source '/home/barix/dotfiles/zsh/zshrc-oh-my.sh'

