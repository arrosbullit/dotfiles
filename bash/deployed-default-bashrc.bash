# Ho faig així perque així quan actualitzo els scripts ho faig
# a dins del repo i no fora! i així puc pujar els canvis al
# repo fàcilment

# Normal version
#source /home/robert/.dotfiles/bash/bashrc.bash

# Version for Linux subsystem
#source /home/robert/.dotfiles/bash/bashrc-extra-ubuntu-in-windows-linux-subsystem.bash

# Home version
source /home/robert/.dotfiles/bash/base_plus_home_additions.bash
