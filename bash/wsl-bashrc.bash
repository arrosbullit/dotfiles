#source /home/robert/.dotfiles/bash/bashrc.bash
AUX="${HOME}/.dotfiles/bash/bashrc.bash"
source $AUX

# Extra stuff... specific of ublox linux subsystem

# Add path to my scripts
export PATH=$PATH:~/my-scripts/ublox
export PATH=$PATH:~/my-scripts/media-related-scripts
export PERL5LIB=/home/robert/local-repos/Perl_Modules_Git



#bind 'set bell-style none'

# It takes to long to to this so I  comment it out.
#/home/robert/my-scripts/ublox/mount-windows-drive-from-linux-subsystem.sh > /home/robert/logs/cron.log 2>&1
# It takes too long..
#/home/robert/my-scripts/ublox/backup-txts-to-git-repo.sh > /home/robert/logs/db-backup.log 2>&1

#export LANG=de_CH.UTF-8
#export LANGUAGE=de

# set DISPLAY to use X terminal in WSL
# in WSL2 the localhost and network interfaces are not the same than windows
# In TMUX, the inner code fails so I wrap it around this "$DISPLAY" = ":0.0"
#if [ "$DISPLAY" = ":0.0" ]; then

#if ! { [ "$TERM" = "screen" ] && [ -n "$TMUX" ]; } then
#
#    if grep -q WSL2 /proc/version; then
#        # execute route.exe in the windows to determine its IP address
#        export DISPLAY=$(route.exe print | grep 0.0.0.0 | head -1 | awk '{print $4}'):0.0
#
#    else
#        # In WSL1 the DISPLAY can be the localhost address
#        if grep -q icrosoft /proc/version; then
#            export DISPLAY=127.0.0.1:0.0
#        fi
#    fi
#fi # not tmux

# To use WSLG
#export DISPLAY=:0


#sudo service dbus start

# Neovim stuff
export PATH=$PATH:~/sw-downloaded
#alias nvim='~/sw-downloaded/202208.nvim.appimage/nvim.appimage'
alias nvim='~/sw-downloaded/neovim/squashfs-root/AppRun'

# Ublox stuff to be able to use Perl_Modules_Git
export UBX_SEC_KEY_FILE=/home/robert/local-repos/Perl_Modules_Git/UBX/sys_sec_keys.json

export PYTHONPATH=$PYTHONPATH:/mnt/c/repos/ublox/asp-pos/python/pyblox:/home/robert/repos/meus/my-python-and-matlab-tests/src/ubx-parsing

# Aixo es per poder obrir el visual studio code des del bash ja que aquest path que està
# configurat al Windows for some reason només el veig al tab inicial del tmux perpo no
# als següents tabs

export PATH=$PATH:/mnt/c/Users/rllu/AppData/Local/Programs/MicrosoftVSCode/bin


## Ho poso al final pq ho mencionen aquí: https://stackoverflow.com/questions/24585261/nvm-keeps-forgetting-node-in-new-terminal-session
## If that doesn't work, make sure in your .bash_profile (or .bashrc or whatever) you don't have anything modifying PATH after source xx/nvm.sh
## Mogut manualment al .profile
## Sense això no va el node
#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# to use the latest version
#nvm alias default node
#nvm use default

# Això només falta quan fas servir nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Per a que el node instal·lat amb nvm sigui trobable
export PATH=$PATH:~/.nvm/versions/node/v22.11.0/bin

# Visual studio code crap. Això ho necessito o si no
# cada cop que faig "code ." obre una fiestra amb el file cli.js
# Cada puta vegada.
# Ho desactivo pq a casa això fa que "code ." o "explorer.exe ." donin
# el error "Invalid argument"
the_hostname=$(hostname)

if [ "$the_hostname" = "ch-thl-lt-rllu1" ] ; then
    echo "Export the thing"
    export WSL_DISTRO_NAME="Ubuntu"
else
    echo "Do nothing"
    echo ""
fi

