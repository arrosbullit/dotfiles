
#source /home/robert/.dotfiles/bash/bashrc.bash
AUX="${HOME}/.dotfiles/bash/bashrc.bash"
source $AUX

# Extra stuff... specific for home

# QMK Tap completion per al sofware que compila keyboard firmwares anomenat
#source ~/qmk_firmware/util/qmk_tab_complete.sh

# To be able to do core dumps
#ulimit -c unlimited
