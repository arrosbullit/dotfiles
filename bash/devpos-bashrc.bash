source /home/rllu/.dotfiles/bash/bashrc.bash

# Add path to Perl testing scripts
export PATH=$PATH:/home/gpstest/ReleaseTestGUT/bin

# Add path to my scripts
export PATH=$PATH:/home/rllu/scripts
export PATH=$PATH:/home/rllu/my-scripts/ublox

export LANGUAGE=de

set -o vi
