# ~/.bashrc: executed by bash(2) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
#HISTCONTROL=ignoreboth
# Robert I change this because I want the history to be remebered for
# commands that start with spaces or tabs as I paste commands I have
# previously written in vim prefixed with tabs
HISTCONTROL=ignoredups

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=100000
HISTFILESIZE=200000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;29m\]\u@\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;29m\]\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
else
    #PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\W\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi
#
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Per a que el terminal no es pengi quan faig control s al vim
stty -ixon


set-title() {
  if [[ -z "$ORIG" ]]; then
    ORIG=$PS1
  fi
  TITLE="\[\e]2;$*\a\]"
  PS1=${ORIG}${TITLE}

  # This is to set the title of the Tmux Window
  CMD="\033k$1\033\\"
  printf $CMD
}

# Kind of alias
function gitone() {
	ssh-agent /bin/bash 
}


function gittwo() {
	ssh-add /home/robert/.ssh/arrosbullit 
    #ssh-add ./ssh/arrosbullit
}

function gitrllu() {
	ssh-add ~/.ssh/rllu
}


# Alias list

# Begin show current branch name
    #alias gitb="git branch --contains"

    # Show branch nane easier way that only works in the root dir
    # Works also with older gits while "git branch -show" works only in new gits
    # alias gitb="cat .git/HEAD"

    # Show branch name convoluted way
    #alias gitbb="git rev-parse --abbrev-ref HEAD"

    # Understandable method that works in newer git versions only
    alias gitb="git branch --show"

    # Show merges only
    # --merges: show merges only
    # %cn: commit name or author %an author name is the same
    # %x09: to add some space, I am not sure
    # %b: body of the commit
    #
    # grep -v "See merge" # Skip lines containing See merge
    # | less # /make it scrollable
    alias gitlogmerge="git log --merges --pretty=format:\"%h%x09%cn%x09%b\" | grep -v \"See merge\" | less"

# End show current branch name

alias gits="git status -uno"
alias gitd="git difftool -d"
alias gitsave="git add -u; git commit -m save"
alias gitp="git add -u; git commit -m save; git push"

# Do not clean clipboard on exit!
#autocmd VimLeave * call system("xsel -ib", getreg('+'))

# alias cdroot="cd /home/barix/oe-core/build/tmp-glibc/deploy/images/barix-ipam400"
# alias cdwww="cd /usr/local/www/"
# alias cdimages="cd $HOME/barix-platform-oe-core/build/tmp-glibc/deploy/images/barix-ipam400"
# alias cdtools="cd /home/barix/oe-core/stuff/meta-qiba/tools"
# alias cdbarix="cd /home/barix/oe-core/stuff/meta-barix"
# alias cdqiba="cd /home/barix/oe-core/stuff/meta-qiba"
# alias cdrescue="cd /home/barix/qiba-platform-oe-core/build/tmp-glibc/deploy/images/barix-ipam400"
# alias cdinstaller="cd /home/barix/qiba-platform-oe-core/build/tmp-glibc/deploy/images/barix-ipam400"
# alias cdscripts="cd /home/barix/repos/barix-notes/scripts"
# alias cdqiba="cd /home/barix/oe-core/stuff/meta-qiba/"
#alias cdrepos="cd /media/rob/data/repos"


# Save history every time I type a command!!!
export PROMPT_COMMAND='history -a'

# Begin Go Paths
    # Director where go downloads packages
    #  root of your workspace
    GOPATH=$HOME/go
    #export GOPATH

    # Add the go bin directory to general path
    #PATH=$PATH:$GOPATH/bin

    # Add this other go env var
    #GOROOT=/usr/bin
    #export GOROOT

    #Move these things to $HOME/.profile
    export GOROOT=/usr/local/go
    #export GOPATH=$HOME
    #export GOPATH=/home/barix/go:/home/barix/repos:/home/barix/repos/informacast/main-informacast
    #export GOPATH=/media/data/go-libs
    #export GOPATH=$/my-go-path
    #export GOBIN=/media/data/go-binaries
    #export GOBIN=~/go/go-binaries
    export PATH=$PATH:/usr/local/go/bin:/usr/.local/bin:$GOBIN:

# End Go paths

# Robert finder
#alias finder=$HOME"/Dropbox/zuhauseundmehr/scripts/finder.sh"
finder () {
	find . -type f -name "*$1*" -print0 | xargs -0 grep "$2"
}
alias finder="/home/barix/repos/robert/my-scripts/finder.sh"

# Robert minicom amb title
alias minicomtitle="set-title minicom; minicom"

alias nau="nautilus"


grepNear() {
 grep -EHn "\b$1\W+(\w+\W+){1,10}$2\b"
}


mygrepFunc() {
# grep -riIn \
#      --exclude-dir=.git \
#      --exclude-dir='build*' \
#      --include='*.bb*' \
#      --include='*.inc*' \
#      --include='*.conf*' \
#      --include='*.py*' \
#      "$@"

#      --include="*.cpp" \
 grep -riIn \
      --exclude-dir=.git \
      --exclude='*.htm' \
      --exclude='*.html' \
      --exclude='*.js' \
      --exclude='*.css' \
      --exclude='a_*' \
      "$@"
 }

alias mygrep="mygrepFunc"

# alias setenv="source /home/barix/Dropbox/zuhauseundmehr/barix/scripts/set-sdk-env-vars.sh"

# Custom cd
c() {
    cd $1;
    ls;
}
alias cd="c"

alias z="zsh"

lsdirFunc(){
    echo "ls -d */";
    ls -d */
}


#alias lsdir="ls -d */"
alias lsdir="lsdirFunc"

#retmux function

function remuxFunc() {
  tmux source-file ~/.tmux.conf 
}
alias remux="remuxFunc"

#fortune softwareengineering | cowsay
#fortune softwareengineering sfw-programming | cowsay
#fortune | cowsay

export BROWSER=firefox

# This is needed so that tmux does not change
# the colors configured in vim
#export TERM="screen-256color"

# Disable trachpad for one second after every key press
#killall syndaemon
#syndaemon -i 0.6 -d

# To have access to some of my scripts
export PATH=$PATH:~/my-scripts
export PATH=$PATH:~/my-scripts/media-related-scripts

# To change Windows directory colors when doing ls
eval "$(dircolors ~/.dotfiles/dir-colors/modified-dircolors.txt)";

# Robert. Posa els executables en negre bold pq el Linux veu tots els 
# Windows files as executables i per defecte executable és verd i això
# fa massa verd en total
LS_COLORS=$LS_COLORS:'ex=1;30'
export LS_COLORS

# Vim mode per editar el que escrius al command line
# Bash vim mode
set -o vi

# Python path added after doing: pip3 install virtualenv
export PATH=$PATH:~/.local/bin

# Enable vim mode
set -o vi


# Extra stuff...

# Neovim stuff
alias nvim='~/sw-installed/neovim/nvim.appimage'

## Mogut manualment al .profile
# Sense això no va el node
#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# Això només falta quan fas servir nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Dropbox-git connection/binding

# Choose default editor
export EDITOR='vim'
export VISUAL='vim'
