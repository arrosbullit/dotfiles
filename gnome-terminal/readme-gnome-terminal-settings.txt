https://askubuntu.com/questions/967517/backup-gnome-terminal

GNOME Terminal itself doesn't provide such an option, so you have to manually
operate on its database.

Beginning with version 3.8 it uses GSettings, which in turn (at least on Linux
systems) uses dconf. It would probably be more elegant to go with the gsettings
tool. Unfortunately I couldn't figure out how to dump all the relevant data
there, let alone restore them. So let's use dconf.

Based on this article, you can dump your settings using:

dconf dump /org/gnome/terminal/ > gnome_terminal_settings_backup-linux-subsystem.txt
Reset (wipe out) the settings before loading a new one (probably not really required):

dconf reset -f /org/gnome/terminal/
Load the saved settings:

dconf load /org/gnome/terminal/ < gnome_terminal_settings_backup.txt
Disclaimer: I haven't tested the restore steps. I recommend that before the reset/load operations you back up your entire dconf database, which is stored in the single file ~/.config/dconf/user, using a simple standard filesystem copy operation (as opposed to some dconf command). In case of problem you can restore it just as easily (maybe from another terminal emulator or the Linux console).
