
# For vim mappings: 
	stty -ixon

# Robert from here and up

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  #export ZSH="/home/barix/.oh-my-zsh"
  export ZSH="/home/barix/dotfiles/zsh/plugins/oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

    #To map control k to cd .. and more
    source ~/dotfiles/zsh/plugins/oh-my-zsh/lib/key-bindings.zsh
    source ~/dotfiles/zsh/keybindings.sh




    # To not type so many numbers when sshing

        function ssh-with-fixed-prefix(){
            ssh root@192.168.11.$1
        }

        alias sache="ssh-with-fixed-prefix"


    function set-title() {
      if [[ -z "$ORIG" ]]; then
        ORIG=$PS1
      fi
      TITLE="\[\e]2;$*\a\]"
      PS1=${ORIG}${TITLE}
    }

    function st(){
      if [[ -z "$ORIG" ]]; then
        ORIG=$PS1
      fi
      TITLE="\[\e]2;$*\a\]"
      PS1=${ORIG}${TITLE}
    }

    # Do not clean clipboard on exit!
    #autocmd VimLeave * call system("xsel -ib", getreg('+'))

    alias cdroot="cd /home/barix/oe-core/build/tmp-glibc/deploy/images/barix-ipam400"
    alias cdrobert="cd /home/barix/projects/robertcur"
    alias cdwww="cd /usr/local/www/"
    alias cdimages="cd $HOME/barix-platform-oe-core/build/tmp-glibc/deploy/images/barix-ipam400"

    # Save history every time I type a command!!!
    export PROMPT_COMMAND='history -a'

        # Director where go downloads packages
        #GOPATH=$HOME/go
        #export GOPATH

        # Add the go bin directory to general path
        #PATH=$PATH:$GOPATH/bin

        # Add this other go env var
        #GOROOT=/usr/bin
        #export GOROOT

    export PATH=$PATH:/usr/local/go/bin

    export GOROOT=/usr/local/go
    export GOPATH=$HOME

    # Robert finder
    alias finder="/home/barix/Dropbox/zuhauseundmehr/scripts/finder.sh"
    # Robert minicom amb title
    alias minicomtitle="set-title minicom; minicom"

    export NODE_PATH=/home/barix/node_modules

    alias nau="nautilus ."


    grepNear() {
     grep -EHn "\b$1\W+(\w+\W+){1,10}$2\b"
    }

    PATH=$PATH:/home/barix/Dropbox/zuhauseundmehr/barix/scripts

    alias cdscripts="cd /home/barix/Dropbox/zuhauseundmehr/barix/scripts"

    alias setenv="source /home/barix/Dropbox/zuhauseundmehr/barix/scripts/set-sdk-env-vars.sh"

    # Custom cd
    cd_and_ls() {
        cd $1;
        ls;
    }
    alias cd="cd_and_ls"

    alias z="zsh"

    alias lsdir="ls -d */"


#Afegir el temps gastat al prompt
    #setopt PROMPT_SUBST
    #set_prompt() {
    #    # Timer: http://stackoverflow.com/questions/2704635/is-there-a-way-to-find-the-running-time-of-the-last-executed-command-in-the-shel
    #    if [[ $_elapsed[-1] -ne 0 ]]; then
    #        PS1+=', '
    #        PS1+="%{$fg[magenta]%}$_elapsed[-1]s%{$reset_color%}"
    #    fi
    #}
    #
    #precmd_functions+=set_prompt
    #
    #preexec () {
    #   (( ${#_elapsed[@]} > 1000 )) && _elapsed=(${_elapsed[@]: -1000})
    #   _start=$SECONDS
    #}
    #
    #precmd () {
    #   (( _start >= 0 )) && _elapsed+=($(( SECONDS-_start )))
    #   _start=-1 
    #}
    #
