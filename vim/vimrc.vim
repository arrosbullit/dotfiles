" Confiture the value of <leader>
" Keep this one at the top jus in case the definition needs to be known before
" using it below
" kw: copy_paste

let mapleader=" "
let maploacalleader=" "

nnoremap <leader><leader> :

" Afegeix coses a partir de here
" Mit 30-10-19
" Menciona el plugin vim-go to be installed by vim-plug
" Plug és un pluggin manager que pel que veig tinc disabled al directori

call plug#begin('~/.vim/plugged')
    " Doc per instal·lar aquests plugins fas: :PlugInstall
    " Doc per actualizar aquests plugins fas: :UpdateRemotePlugins
    " Plug 'fatih/vim-go'
    " No l'instl·lo així crec
    "Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
    Plug 'dhananjaylatkar/cscope_maps.nvim' " cscope keymaps
call plug#end()

" Abbreviation és com un corrector automàtic
" Quan escrius HC en minúscules, ell ho reemplaça per un html comment
:ia hc <!--<CR>--><Esc>

" modeline allows to write vim settings on top
" of the files you edit in header settings vim
" Enable custom settings which are sent in the files
" you edit
" Exemple. Escriu això al top del file. vim header
" vim: expandtab tabstop=4 shiftwidth=4
set modeline

" Maps control space to autocompletion
" Not needed anymore. Control n does this as well
" https://stackoverflow.com/questions/23189568/control-space-vim-key-binding-in-normal-mode-does-not-work#23190040
"inoremap <NUL> <C-x><C-i>
"nnoremap <NUL> i<C-x><C-i>

" To make tmux use the right colors
set t_Co=256


" Cursor shape based on Mode Settings
" Cursor settings:
"  1 -> blinking block
"  2 -> solid block 
"  3 -> blinking underscore
"  4 -> solid underscore
"  5 -> blinking vertical bar
"  6 -> solid vertical bar
"let &t_SI.="\e[5 q" "SI = INSERT mode
let &t_SI.="\e[2 q" "SI = INSERT mode
" Disabled in u-blox server ublox
"let &t_SR.="\e[4 q" "SR = REPLACE mode
"let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
let &t_EI.="\e[2 q" "EI = NORMAL mode (ELSE)

" Putting the above in your vimrc keeps track of the
" cursor position when in INSERT mode. When exiting INSERT
" mode, it will move the cursor ahead one column if the
" cursor position has changed after leaving insert. 
let CursorColumnI = 0 "the cursor column position in INSERT
autocmd InsertEnter * let CursorColumnI = col('.')
autocmd CursorMovedI * let CursorColumnI = col('.')
autocmd InsertLeave * if col('.') != CursorColumnI | call cursor(0, col('.')+1) | endif

" Redefine control arrow to go to end of current word instead
" of beginning of next word!!!
"nnoremap <C-Right> e
"vnoremap <C-Right> e

" Begin Combo to insert current date
    "nnoremap <F5> "=strftime('%a %d-%m-%y %H:%M') <CR>Pa
    "nnoremap <F5> "=strftime('%a %d-%m-%y') <CR>Pa
    " Print date and time
        nnoremap <F5> "=strftime('%a %d-%m-%y %H:%M') <CR>Pa
        inoremap <F5> <C-R>=strftime('%a %d-%m-%y %H:%M') <CR>
        inoremap <F6> <C-R>=strftime('%H:%M') <CR>
    "" Print only time
    "    nnoremap <F5> "=strftime('%d-%m-%y %a') <CR>Pa
    "    inoremap <F5> <C-R>=strftime('%d-%m-%y %a') <CR>
    "    inoremap <F6> <C-R>=strftime('%d-%m-%y ') <CR>
" End Combo to insert current date

" Disable F4... no funciona
"nunmap <F4>
"iunmap <F4>

"foo.bar
" Stop highlighting trailing whitespace for Go files in Vim
" https://stackoverflow.com/questions/40945136/stop-highlighting-trailing-whitespace-for-go-files-in-vim
let g:go_highlight_trailing_whitespace_error=0

" Disable shit
map q: <Nop>
nnoremap Q <nop>

" Mappings for :cnext, :cprev, :cnfile, :cpfile
" nmap <leader>c :cnext<CR>
" nmap <leader>C :cprev<CR>

" Copy or yank current full file name to register +
" Ojo, conflicts amb el mapping de sobre
"nnoremap <leader>path :let @+ = expand("%:p")
cnoremap ypath let @+ = expand("%:p")

" Doc Print current full filename
" :read! echo %  <--- escriu el file name. % és un registre on hi ha el file name.
" :read! echo %:p <--- escriu el full filename (inlclou el path )

" Select what I have just pasted
nnoremap gp `[v`]

" Go to beginning of line in normal mode,
" delete until column 0 and go to inser mode
nnoremap <leader>b ^d0i

" Open gedit with current file
cnoremap  gedit !gedit %:p &<cr><cr>

" Exit visual mode without delay
" https://stackoverflow.com/questions/15550100/exit-visual-mode-without-delay
set timeoutlen=1000 ttimeoutlen=0
" one esc -> two esc; because two esc are needed to go
" from visual to normal. Maybe not needed
"vnoremap <Esc> <Esc><Esc>

" Move around splits not using control w
" Intento reemplaçar això pel plugin tmux navigator
" nnoremap <C-j> <C-w>j
" nnoremap <C-k> <C-w>k
" nnoremap <C-l> <C-w>l
" nnoremap <C-h> <C-w>h

" Split documentation
" Max out the height of the current split
" ctrl + w _
" Max out the width of the current split
" ctrl + w |
" Normalize all split sizes, which is very handy when resizing terminal
" ctrl + w =
" Swap top/bottom or left/right split
" Ctrl+W R
" Help splits
" :help splits

" Mark at line 80
	" Change color of line that indicates desired text width
	" :help ctermbg // to see list of colors
	"highlight ColorColumn ctermbg=DarkGrey
	"highlight ColorColumn ctermbg=LightYellow
	" match this regex and apply this color
	" anything you find at the 81st column highlight it and the 100 is high priority
	" call matchadd('ColorColumn', '\%81v', 100)
	"http://stackoverflow.com/questions/235439/vim-80-column-layout-concerns
	" recorda que colorcolumn = cc
	"set colorcolumn=80

" Go to current file path 
" nnoremap <leader>cd :cd %:p:h<cr>

" Status line variants status bar
	" Variant one
		" Enable status line always. Ho he hagut de treure pq paro de clicar i arrastrar la
		" status line accientalment i dona molt pel sac
		" set laststatus=2
		" Mostra menys la status line :(	
		 set laststatus=1
		" now set it up to change the status line based on mode
		if version >= 700
		  au InsertEnter * hi StatusLine term=reverse ctermbg=5 gui=undercurl guisp=Magenta
		  au InsertLeave * hi StatusLine term=reverse ctermfg=0 ctermbg=2 gui=bold,reverse
		endif
	" Variant 2
		" Change status bar color to make active window more obvious
		"highlight StatusLineNC cterm=bold ctermfg=white ctermbg=darkgray
		" do :help status line for help
		" F = full file name c = colunm number, l = line number , p = percentage, 
        " t = file name without path
		"set statusline=%c,%l,%p%%
		"set statusline=%F\ %c,%l,%p%%
		"set statusline=%t\ %c,%l,%p%%
		"set statusline+=\ %y
		"set statusline+=\ %{''.(&fenc!=''?&fenc:&enc).''}
		"set statusline+=\ %{(&bomb?\",BOM\":\"\")}
		"set statusline+=\ %{&ff}

	" I dont know wtf is this but it hase to before showcmd or else showcmd
	" does not work. Also required by the Project plugin  I believe.
	set nocompatible
    " Show commands typed in normal mode.
    set showcmd

" I do not want so many uppercases
" Version 7 of vim does got accetp this ??
"tnoremap <C-w>n <C-w>N
"tnoremap <C-n> <C-w>N

" Control e to go to the end of the dline
" and Control a to go to the beginning of the line
" go to principi, go to final
" Desactivat pq per anar al principi faig 0 i w
" i per anar al final faig control r. " nnoremap <C-e> $
" nnoremap <C-a> ^
" inoremap <C-e> <Esc>$i
" inoremap <C-a> <Esc>^i

" Undo this pecause it does not work out for me
" Exchange v and V in normal mode
"nnoremap v V
"nnoremap V v

" PageUp PageDown less of a jump
nnoremap <PageUp> 10k
nnoremap <PageDown> 10j
inoremap <PageUp> <Esc>10ki
inoremap <PageDown> <Esc>10ji

" Mapping <Shift>-Arrows to selecting characters/lines
" https://stackoverflow.com/questions/9721732/mapping-shift-arrows-to-selecting-characters-lines
set keymodel=startsel,stopsel
"set keymodel=startsel

" Move by words mapping
"nnoremap L w
"nnoremap H b
"vnoremap L e
"vnoremap H b

" Make control d delete a line like in gedit
noremap <c-d> "_dd

" Select line skipping initial white spaces and sipping final <CR>
noremap <leader>y ^v$hy

" use ,F to jump to tag in a vertical split
nnoremap <silent> ,F :let word=expand("<cword>")<CR>:vsp<CR>:wincmd w<cr>:exec("tag ". word)<cr>

" use ,gf to go to file in a vertical split
nnoremap <silent> ,gf :vertical botright wincmd f<CR>

" Paste plus indent and formating with the = command
noremap <leader>p :set paste<CR><bar>p=`]<C-o><bar>:set nopaste<CR>

" Go to next buffer 
noremap <leader>n :bn<CR>
" Go to previous buffer, "a" memotècnic per anterior
" Col.lisió amb le meu paste special si faig servir la p miúscula
noremap <leader>a :b#<CR>

" Edit file quickly
" I only trump the action "go to end of word"
" noremap E :edit!

" Source .vimrc reload vimrc
noremap S :source ~/.vimrc<CR>

" Delete buffer quickly
" Triple dd slows down dd that I use a lot so I have to discard this one
" noremap ddd :bd<CR>
noremap X :bd<CR>

" Open line and go back to normal mode
noremap o o<ESC>
noremap O O<ESC>

" Quickly open vimrc.vim. Type vimrc maps to full file name.
cnoremap vimrc ~/.dotfiles/vim/vimrc.vim

" Quickly open dotfiles. Typing dotfiles maps to full file name. 
"cnoremap dot  ~/dotfiles

" Comment code after visual selection and colon
"cnoremap comenta :s/^/#/ <bar>:noh
" No ho faig servir mai.
" cnoremap comenta :s/^/\/\// <bar>:noh

" Uncomment code after visual selection and colon
"cnoremap uncomenta :s/^.// <bar>:noh
" No ho faig servir mai.
" cnoremap uncomenta :s/^..// <bar>:noh

" Tabularize shortcut
cnoremap tabu Tabularize /:

" Map ñ to : and the other to /
" It no loger works because spansish n has become a /
" Ho comento tot pq faig servir un remaping a nivell de keyboard.
    " In normal mode
    "nnoremap ñ :
    "nnoremap <leader>c :

    "nnoremap Ñ /
    " In insert mode
    "inoremap Ñ :
 
    "inoremap Ñ /
    " In command mode
    "cnoremap Ñ :

    "cnoremap Ñ /
    " In visual mode
    "vnoremap Ñ :
 
    "vnoremap Ñ /

" Configure char to show when tabs or trailing spaces
"set listchars=tab:!·,trail:·
" Enable show of tabs and trailing chars
" set list 
" Disable show of tabs and trailing chars
" set nolist 

" per tancar un buffer amb control c sense trencar l'split window
" close window close pane
" nnoremap <C-c> :bp\|bd #<CR>
" I change to control x to make it the same as in tmux
" Entra en conflicte amb el auto completion
" nnoremap <C-x> <C-w>c 

" Per sortir ràpid amb control + q

" Begin Crashes in Windows terminal
    inoremap <c-q> <Esc>:q<CR>
    noremap <c-q> <Esc>:q<CR>
" End

" La meva manera de fer scroll
noremap K <c-Y>

noremap J <c-E>
"noremap J <c-D>

"vim automatic resize when changing window
"set winwidth=80
"set winheight=38

" begin copy_paste
    " Això és per fer y i que es copii al clipboard general!!
    " Requreix que el vim ho suporti tb. Verue secció "clipboard in vim"!!
    " https://stackoverflow.com/questions/11489428/how-to-make-vim-paste-from-and-copy-to-systems-clipboard
    " To make all copy paste uses system clipboard
    " I think
    " Ojo! quan canvies això tb has de canviar el TextYankPost * o TextYankPost +
    " una mica més avall!!!!
    "set clipboard=unnamed
    
    " Això funciona en neovim. Trobat fent :help clipboard
    " També he installat win32yank.exe en Windows i ho posat al path
    " de Windows cosa que fa que es vegi al WSL i ho pots comprovar fent
    " win32yank.exe
    set clipboard+=unnamedplus

    " Use windows clipboard copy paste vim WSL
    " https://waylonwalker.com/vim-wsl-clipboard/
    " if system('uname -r') =~ "microsoft"
    "      augroup Yank
    "         " Crec que això elimina o reseteja els autocommands
    "          autocmd!
    "
    "          "The TextYankPost event will be triggered after text has been yanked or deleted. See :h TextYankPost for more help.
    "
    "          " Fent servir el register *
    "          "autocmd TextYankPost * :call system('/mnt/c/Windows/System32/clip.exe ',@")
    "
    "          " Faig servir aquest:
    "          "autocmd TextYankPost + :call system('/mnt/c/Windows/System32/clip.exe ',@")
    "
    "          " Una altra versió lleugerament diferent que ha passat a funcionare a Windows?
    "          let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
    "          autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
    "      augroup END
    "  endif
" end copy_paste

" Disable beep and flash in gvim in $VIM/_vimrc
set vb t_vb=

"colorscheme desert
"colorscheme koehler // Cannot see filename in the status bar
"colorscheme default
" A la nit va bé aquest
"colorscheme industry
" delek colorscheme el poso pq sino el visual highlight no em funciona
" en la ubuntu virtual subsystem for linux
"colorscheme delek

" Guide matrix guideline
" Set cursorcolumn vertical line. Només per quan edites PlantUML o Python
    "set cursorcolumn
	set cursorline

    " Disable vertial line/cursorcolumn from normal mode
    "set cursorcolumn!

	""highlight CursorLine   cterm=NONE ctermbg=darkred ctermfg=white
	"set cursorcolumn
	""highlight CursorColumn term=underline guibg=#e1ff35 cterm=underline
	""highlight CursorColumn term=NONE guibg=#e1ff35 cterm=NONE

    " Use this vertical/cursorcolumn line color for Python 
    " Set the color of of the vertical line/cursorcolumn: 
    " ctermbg=120 #cterm background = light green
    " Source for color list: https://vim.fandom.com/wiki/Xterm256_color_names_for_console_Vim
    " Remve the underline:
    " cterm=NONE
	highlight CursorColumn ctermbg=120 cterm=NONE

"En Linux la lletra la configures al terminal i no al vim.
"Una de bona és la "Ubuntu Mono 13"

" Per consultar, show un setting
" set algo?
" tabs and indentation
  set tabstop=4     " Number of spaces that make a tabulator. (ts) for short
  set shiftwidth=4  " Number of spaces when you select and do > to indentate. (sw) for short.
  set expandtab     " Always uses spaces instead of tab characters (et).
  set autoindent    " Copy indent from current line when starting a new line.
  set smarttab      " Inserts blanks on a <Tab> key (as per sw, ts and sts).
  set softtabstop=4 " Pressing backspace deletes all spaces until prev. column
                      " Disabled this shit because it is fucking werid to press
                      " one backspace and delele more than one empty space
                      " And enabled again because when the autotmatic indent
                      " tation is not smart enough I have to to backspace
                      " many times and it tires me. This happesn in my txt 
                      " files.
                      " And disabled again because it does't fucking work
                      " in my txt files
  "set softtabstop=0 " Number of spaces a <Tab> counts for. When 0, featuer is off (sts).

	" Retab documentation
	" :retab            " Replace existing tabs by spaces
	" :%retab!          " Retabulate the whole file
	" set list          " Show tabs!


   set breakindent   " Show wrapped lines indented!
   "set smartindent
	set nosmartindent
                     " smart indent no em va bé amb javascript 
                     " mother fucker. this prevents me from indenting lines
                     " starting with # " when I use >  to indent
   set autoindent
                     " autoindent funciona una mica millor amb javascrit
                     " I enable this to indent automatically when pressing
                     " return


"set runtimepath^=~/.vim/bundle/ctrlp.vim

" Automatic text wrap at column number n. Útil per escriure blog posts.
" Si seleccciones in text i fas gq (go quality) fa servir el tw (text wrap)
" per posar carry returns.
"set tw=80

" Highlight la searched word
:set hlsearch
" REC :noh to remove highlihth after search
" Incremental search disabled pq el nvim el té enabled per default
:set incsearch!
" Make delete key in Normal mode remove the persistently highlighted matches
nmap <silent>  <BS>  :nohlsearch<CR>
" Highlight trailing white space
	" ACTIVA
	" highlight ExtraWhitespace ctermbg=red guibg=red
	" match ExtraWhitespace /\s\+$/
	" :match // Desactiva el highlight 
	highlight ExtraWhitespace ctermbg=red guibg=red
	match ExtraWhitespace /\s\+$/

	" DESACTIVA
	" Desactivo el trailing white space highlight en entrar un txt
    au BufEnter *.md,*.txt :match
	" Activo el trailing white space highlight en entar un code file
	au BufEnter *.c,*.h,*.cpp,*.xml,*.XML :match ExtraWhitespace /\s\+$/
	"au BufEnter *.c,*.h,*.cpp :match
    " Activo el parser dels errors del copiler per quan fas :make
	" au BufEnter *.py :compiler pyunit


" Relative Number: vol dir que la línia actual és sempre la zero.
":set relativenumber
:set norelativenumber

" Show line numbeR
:set number

" Pathogen es per a poder instal·lar plugins
" al dirèctament al directori .vim/bundle
execute pathogen#infect()

" Begin Control p settings
    " Do not change working directory
    let g:ctrlp_working_path_mode = 0
    " symbolic links
    let g:ctrlp_follow_symlinks = 1
    let g:ctrlp_by_filename = 1
    " Keep cache after closing vim
    let g:ctrlp_clear_cache_on_exit = 0
    " Accelerate control p according by using ag to search files:
    " https://stackoverflow.com/questions/21346068/slow-performance-on-ctrlp-it-doesnt-work-to-ignore-some-folders

    " No ho sé utilitzar bé
    " let g:ctrlp_custom_ignore = {
    " \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    " \ 'file': '\v\.(exe|so|dll)$',
    " \ 'link': 'SOME_BAD_SYMBOLIC_LINKS',
    " \ }

    " let g:ctrlp_custom_ignore = {
    " \ 'dir':  'doc',
    " \ }

    " No va
    "set wildignore+=*/doc/*        " Linux/MacOSX

    " El paràmetre --follow  és per trobar els symbolic links
    if executable('ag')
      " let g:ctrlp_user_command = 'ag %s -l --follow --nocolor -g ""'
      " --nogroup
        let g:ctrlp_user_command = 'ag %s -l --follow -i --nocolor --hidden
          \ --ignore .git
          \ --ignore .svn
          \ --ignore .hg
          \ --ignore .DS_Store
          \ --ignore doc
          \ --ignore "**/*.pyc"
          \ --ignore "*.d"
          \ --ignore "*.o"
          \ --ignore "*.gcno"
          \ -g ""'
    endif

" End of control p settings

" He itentat remapejar control p i no hi ha hagut
" manera... o sigui que he acabat remapejant <leader> o com aquest
" tio: https://www.youtube.com/watch?v=8XGueeQJsrA
"nnoremap <leader>o :CtrlPMRU<CR>
"mmm .... ara ho pillo tb ho puc fer així... però no va 
"bé... no troba els buffers oberts amb aquesta merda...
"un moment, crec que sí que funciona però per veure-ho has
" de tenir dos buffers oberts!
" let g:ctrlp_map = '<c-p>'
" Desabilito el mixed pq fot coses rares. M'ensenya fitxers
" que no existeixen. Pq crec que ensenya els Most REcently used
" let g:ctrlp_cmd = 'CtrlPMixed'
"let g:ctrlp_cmd = 'CtrlPMRU'
" I ara, amb CtrlPMixed, això no cal però ho deixo pq pot
" ser útil.
" Per obrir buffers oberts provo això
nnoremap <leader>o :CtrlPBuffer<CR>
" Fuzzy search al current buffer
nnoremap <leader>/ :CtrlPLine<CR>

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

"Nerdtree opener with F2
nmap <silent> <F2> :execute 'NERDTreeToggle ' . getcwd()<CR>

"Shortcuts del NERDTree

" Find open file in NERDTree
nmap ,m :NERDTreeFind<CR>

" Open NERDTree
nmap ,n :NERDTree<CR>

set backspace=2 " make backspace work!

" Autocomplete brace bracket parenthesis
""ino " ""<left>
""ino ' ''<left>
""ino ( ()<left>
""ino [ []<left>
""ino { {}<left>
ino {<CR> {<CR>}<ESC>O

" Autocomplete suggest words from dictionary only if set spell is set
" Rec Show completeion settings: :set complete
" Aquest i el coc conqueror of completion se solapen!
" set complete=.,w,b,u,t,i,kspell

" Control + s per salvar en insert mode i anar normal mode
" Ojo! El control + s sol penjar els terminals. Per evitar-ho has
" de posar això al .bashrc stty -ixon
inoremap <c-s> <Esc>:w<CR>
noremap <c-s> <Esc>:w<CR>

" How to delete not cut -> no copiar al registre quan deletes
" Usat quan vull reemplaçar un text a vairs llocs pastejant-hi
" la mateixa cosa
" http://stackoverflow.com/questions/11993851/how-to-delete-not-cut-in-vim
" This convers d into delete line and save in unused buffer

" prevent avoid disable yanking when deleting delete evitar
nnoremap d "_d
vnoremap d "_d
"vnoremap p "_dP
" prevent avoid disable yanking when changing replacing
nnoremap c "_c
vnoremap c "_c
" prevent avoid disable yanking when deleting
nnoremap x "_x
"vnoremap x "_x

" What the fuck was this?? set hidden
" Answer:
" I am using FuzzyFinder with vim to open files and switch between buffers. It
" works like a charm except when the current file I am working on has some
" changes. Vim wouldn't let me switch the buffer till I save it:
" "E37: No write since last change (add ! to override)".
" Is there a way of suppressing this warning unless I am quitting the editor? All
" I want to do it switch to a different buffer for referencing some code and
" switch back.
" Answer:
" Use the :set hidden option and vim will hide the buffer until you come back to
" it.
set hidden

" ignore case
" but also smartcase to not ingore case if you use some uppercase
set ignorecase smartcase


" copy_paste
" Do not clean clipboard on exit! "
" Keep clipboard on exit
" Do not clear clipboard on exit
" Si no funciona una potser funciona l'altra manera. Dos maneres:
autocmd VimLeave * call system("xsel -ib", getreg('+'))
autocmd VimLeave * call system("xclip -selection clipboard -i", getreg('+'))


" Begin Mouse
	" Enable mouse in vim in tmux
	" Disable pq neovim es queixa
    if !has('nvim')
        "set ttymouse=xterm2
        set ttymouse=xterm
        set nocompatible
    endif
	" In .tmux.conf add set -g mouse on

	" Enable mouse
    " Diable mouse pq diuen que afecta al copy paste
	set mouse=a
" End Mouse


" Go to next tab
" Never used
" noremap <F7> :tabn<CR>

" Go to previous tab
" Never used
"noremap <F8> :tabp<CR>

set nobackup       "no backup files
set nowritebackup  "only in case you don't want a backup file while editing
set noswapfile     "no swap files

":terminal

" Indetnation based highlight
" Creating a mapping to turn it on and off:
" El tenia on
" map <leader>l :RainbowLevelsToggle<cr>

" Enable highlighting for syntax
syntax on
" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
"filetype plugin indent on

" on open file commands
" Or automatically turning it on for certain file types:
" au FileType txt,javascript,python,php,xml,yaml,md :RainbowLevelsOn
"au BufReadPost,BufNewFile *.md,*.txt set tw=79
"au BufReadPost,BufNewFile *.md,*.txt :RainbowLevelsOn

" Temporary disable pq crec que aquest és responsable de la lentitud en
" quan vaig a un altre split
" au BufEnter *.md,*.txt :RainbowLevelsOn
" au BufLeave *.md,*.txt :RainbowLevelsOff

" Set tabstop to 4 for Python files just for backward compatibility
" with files that were made using tabs
au BufEnter *.py :set tabstop=4
" au BufEnter *.py :set cursorcolumn
" Configura cosa de python que em permet fer :make i anar als errors com
" si fos en C.
au BufEnter *.py :compiler pyunit

" Use spaces for HTML  and CGI files
"au BufEnter *.cgi :set tabstop=4
"au BufEnter *.cgi :set shiftwidth=4
"au BufEnter *.cgi :set expandtab 
"au BufEnter *.html :set tabstop=4
"au BufEnter *.html :set shiftwidth=4
"au BufEnter *.html :set expandtab 


"============================================================================
" :set norelativenumber
" Set up persistent undo (with all undo files in one directory)
"============================================================================
if has('persistent_undo')
  set undofile
endif
set undodir=$HOME/.VIM_UNDO_FILES
set undolevels=5000

"============================================================================
" When completing, show all options, insert common prefix, then iterate
"============================================================================
"set wildmode=list:longest,full

"============================================================================
" Use arrow keys to navigate after a :vimgrep or :helpgrep
"============================================================================

"    nmap <silent> <RIGHT>         :cnext<CR>
"    nmap <silent> <RIGHT><RIGHT>  :cnfile<CR><C-G>
"    nmap <silent> <LEFT>          :cprev<CR>
"    nmap <silent> <LEFT><LEFT>    :cpfile<CR><C-G>

    nmap  <RIGHT>         :cnext<CR>
    nmap  <LEFT>          :cprev<CR>

" Enable omnicompletion
" To use omni completion, type <C-X><C-O> while open in Insert mode. If
" matching names are found, a pop-up menu opens which can be navigated using
" the <C-N> and <C-P> keys. 
" OJO! En C necessites fer ctags -R per l'omnicomplete
"filetype plugin on
"set omnifunc=syntaxcomplete#Complete

" Scrolling in vim autocomplete box with jk movement keys
" Em veig obligat a desactivar això pequè em passa que amb
" YouCompleteMe surt el pop up automàticament i si continuo
" escribint i hi ha una j aleshores fa servir les suggestions
" del pup-up quan el que jo volia és escrire el què em doni la
" gana
"inoremap <expr> j ((pumvisible())?("\<C-n>"):("j"))
"inoremap <expr> k ((pumvisible())?("\<C-p>"):("k"))

" Enable YouCompleteMe for text files
" YouCopleteMe documentation
" Vas escribint i van sortint pop-ups. Per navegar pels pop-ups, fest TAB o
" Shift TAB. Per tancar el pop-up o bé espai o bé Esc.
"let g:ycm_filetype_blacklist={'txt':0}
"let g:ycm_filetype_blacklist={'txt':0, 'text':0}
"let g:ycm_filetype_whitelist = { 'cpp': 1, 'c': 1, 'python':1, 'txt': 1, 'text': 1 }
"let g:ycm_filetype_whitelist = { 'cpp': 1, 'c': 1, 'python':1, 'txt': 1, 'text': 1 }

" Autocomplete deocomplete shit
"if has('nvim')
"  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
"  Plug 'Shougo/deoplete.nvim'
"  Plug 'roxma/nvim-yarp'
"  Plug 'roxma/vim-hug-neovim-rpc'
"endif
"let g:deoplete#enable_at_startup = 1
"

" Make vim search the ctags file in parent directories unti it finds one
set tags=tags;
" Use tags from the present directory
"set tags=./tags

" Make vim search files I want to open with "gf" up and down the cur dir
" Ojo això alenteix vim! slow problem! Solució aquí:
" Can be improved. See this: https://stackoverflow.com/questions/52123349/vim-how-to-get-path-to-search-up-and-down-a-directory-tree
set path+=**;

" keep more context when scrolling off the end of a buffer
set scrolloff=3

" If a file is changed outside of vim, automatically reload it without asking
set autoread

" Diffs are shown side-by-side not above/below
set diffopt=vertical

" Això és per solucionar l'error:
" CONVERSION ERROR in vim
" que passa quan intentes salvar un document que te caràcters raros
set encoding=utf-8

nnoremap <leader>q @q

" Install or enable matchit plugin. Comes from doing :help matchit-install.
" comes from here https://stackoverflow.com/questions/11851320/matchit-skips-to-next-list-item-in-html-instead-of-closing-tag
" This shit is a requirement from matchit
filetype plugin on
" Disabled in u-blox server ublox
"packadd! matchit

" Search search find word under cursor across files busca buscar
	" Opció 1
	" map <f4> :execute "vimgrep /" . expand("<cword>") . "/j **" <bar> cw<cr>
	" map <f4> :execute "vimgrep /" . expand("<cword>") . "/j **/*.go **/*.py" <bar> cw<cr>
	" Opció 2
	nnoremap <leader>grep yiw:vimgrep /\C<c-r>"/  **/*.c **/*.cpp **/*.h **/*.m **/*.txt ../**/*.go
	" Where:
	" 	yiw: yank inner workd to register "
	" 	\C: case insensitive
	" 	<c-r>": paste contents of register "
    "   | copen vol dir que obri una finestreta de results named quick fix window o similar
    " Opció 3
    " search Buscar només al fitxer actual
    " nnoremap <leader>fgrep yiw:vimgrep /\C<c-r>"/  % | copen
    " Primer fas yiw per seleccionar el que vols i després fas :fg
	" Desactivat pq fg és algo que escric sovint p.ex cfg
    " cnoremap fg vimgrep /\C<c-r>"/  % 

" Doc only: search workd across files
" https://seesparkbox.com/foundry/demystifying_multi_file_searches_in_vim_and_the_command_line
    ": grep word *.m
    " Per obrir la finestra on hi ha la cerca
    ":copen
    " Per tancar la finestra on hi ha la cerca 
    ":cc  o bé X

" Map omnicompletion to control x control x instead of control x control o
" inoremap <C-x>x  <C-x><C-o>

" Autocompletion with tab nine plugin
" set rtp+=~/.vim/bundle/tabnine-vim

" vim-go shit golang
" Els mappings per defecte per anar a la implementació de la func
" no funciona! Però si ho desabilito puc fer servir ctags instead.
" Ha passat a funcionar misteriosament??!! per això comento la línia
" I ha tornat a deixar de funcionar... o sigui que ho activo again
let g:go_def_mapping_enabled = 0
" Disable more vim-go fucking mappings
let g:go_doc_keywordprg_enabled = 0
" vim-go to use gopls to go to definition and to info
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'

"" coc_block begin Start of conqueror_of_completion
"" Shit Plugin Conquer of Completion. El intellisense no va. Només em va la
"" suggestions de paraules un cop he començat a clicar

    " Avoid warning message when using vim with coc which happens
    " when I do git commmit seguint el consell:
    " Please do not report bugs unless you're using at least Vim 9.0.0438 or Neovim 0.8.0.
    let g:coc_disable_startup_warning = 1

    " coc autocompletion disable. Set variable
    "let b:coc_suggest_disable=1
    "let b:coc_suggest_disable=0
    "let b:coc_enabled=1

    " Més disable prémer :CocConfig i després escriure això:
    " autocmd FileType markdown let b:coc_suggest_disable = 1

    """ https://github.com/neoclide/coc.nvim
    """ Some Doc
    " :CocDiagnostics mostra una llista dels errors trobats!

    """" More stuff being tested
    " Some servers have issues with backup files, see #649.
    set nobackup
    set nowritebackup

    " Give more space for displaying messages.
    " set cmdheight=2

    " Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
    " delays and poor user experience.
    set updatetime=300

    " Don't pass messages to |ins-completion-menu|.
    set shortmess+=c

    " Highlight the symbol and its references when holding the cursor.
    " autocmd CursorHold * silent call CocActionAsync('highlight')

    " Add `:Format` command to format current buffer.
    " command! -nargs=0 Format :call CocActionAsync('format')

    """

    "coc_tab Begin: navigate in completion list using tab or shift tab
    " { Navigate in completion list using tab or shift tab

        "{ TAB va a la next suggestion

            " Disable normal vim completions
            set complete=

            "" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.

            " Source: https://www.reddit.com/r/vim/comments/x3mbjp/when_using_coc_the_autocomplete_feature_goes_to/
            function! CheckBackspace() abort
              let col = col('.') - 1
              return !col || getline('.')[col - 1]  =~# '\s'
            endfunction

            inoremap <silent><expr> <Tab>
                  \ coc#pum#visible() ? coc#pum#next(1) :
                  \ CheckBackspace() ? "\<Tab>" :
                  \ coc#refresh()
        "}

        "{ Use Enter to acceot coc suggestion
            inoremap <silent><expr> <CR>
                \ coc#pum#visible() ? coc#pum#confirm() :
                \ "\<CR>"
        "}

        " Això no va al WSL de la feina
        "inoremap <expr> <TAB> coc#pum#visible() ? "\<C-n>" : "\<Tab>"
        "inoremap <expr> <S-TAB> coc#pum#visible() ? "\<C-p>" : "\<S-Tab>"

        " Això tampoc va al WSL de la feina
        "inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<Tab>"
        "inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<S-Tab>"

        " TAB applica la current selection
        "inoremap <expr> <Tab> pumvisible() ? "\<C-y>" : "\<Tab>"

    " coc_tab End: navigate...


    " RECORDA Per triar la primera suggerècia pots fer servir control-y!! <C-y>
    " Debug per verue si C-y està mapped a alog diferent fes :verbose imap <C-y>

	" Begin Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.

        " Disabeld pq a vegades premo enter per saltar a la línia següent
        " i no per acceptar una suggerència i amb això passa que
        " Disabled pq quan premo return és pq vull canviar de línia ino acceptar una suggerècia

        " Coc only does snippet and additional edit on confirm.
        "inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
        " This version works with neovim  0.8.1

        " Això no va al WSL de la feina. o pot ser si no se
        "inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
        "                          \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

        " Això no va al WSL de la feina. Pues sí que va.inoremap <silent><expr> <CR><CR> coc#pum#visible() ? coc#pum#confirm()
        " inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()

        " pumconfirm() no va al WSL de la feina
        "inoremap <silent><expr> <CR> pumvisible() ? pumconfirm()
        "                          \: "\<C-g>u\<CR>\<c-r>=on_enter()\<CR>"

        " Or use `complete_info` if your vim support it, like:
        "inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
    " End

	" Remap keys for gotos. Conqueror doc. Coc Conqueror of Completion
	nmap <silent> gd <Plug>(coc-definition)
	nmap <silent> gy <Plug>(coc-type-definition)
	nmap <silent> gi <Plug>(coc-implementation)
	nmap <silent> gr <Plug>(coc-references)

    " Highlight the symbol and its references when holding the cursor.
    autocmd CursorHold * silent call CocActionAsync('highlight')

" coc_block End of conqueror_of_completion

" Funcions bones
" Usage:
" :call UseTabs()
" :call UseSpaces()
" Show tabs or spaces
" set list
" set list!

function! UseTabs()
  set tabstop=4     " Number of spaces that make a tabulator. (ts) for short
  set shiftwidth=4  " Number of spaces when you select and do > to indentate.
  set noexpandtab   " Always uses tabs instead of space characters (noet).
  set autoindent    " Copy indent from current line when starting a new line (ai).
endfunction


" C programming disable automatic comment insertiong
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" C programming  style
" autocmd BufNewFile,BufRead *.cpp set formatprg=astyle\ -T4pb

" C style with G=gg
autocmd FileType c,cpp setlocal equalprg=clang-format

function! UseSpaces()
  set tabstop=4     " Number of spaces that make a tabulator. (ts) for short
  set shiftwidth=4  " Number of spaces when you select and do > to indentate.
  set expandtab     " Always uses spaces instead of tab characters (et).
  "set softtabstop=0 " Number of spaces a <Tab> counts for. When 0, featuer is off (sts).
  set autoindent    " Copy indent from current line when starting a new line.
  set smarttab      " Inserts blanks on a <Tab> key (as per sw, ts and sts).
endfunction

function! UseTwoSpaces()
  set tabstop=2     " Size of a hard tabstop (ts).
  set shiftwidth=2  " Size of an indentation (sw).
  set expandtab     " Always uses spaces instead of tab characters (et).
  set softtabstop=2 " Number of spaces a <Tab> counts for. When 0, featuer is off (sts).
  set autoindent    " Copy indent from current line when starting a new line.
  set smarttab      " Inserts blanks on a <Tab> key (as per sw, ts and sts).
endfunction

" Ublox coding guideline style prohibit an opening brace
" at the end of a line. This regex:
" \S non-blank char
" \+ 1 or more
" \s blank char
" * 0 or more
" { jsut the brace
function! CG1()
    :normal /\S\+\s*{
    " This thing is to be able to
    " repeat the search with "n"
    let @/='\S\+\s*{'
endfunction


" Canviar això depenent del current backtround del terminal
" set bg=dark
set bg=light

" Begin ctags 
" Al command line cal haver fer ctags -R i crea el fitxer tags.
" ctags go to declaration
nnoremap <leader>g  <C-]>
" End ctags

" Doc
" Recorda que gF és per open file under cursor que inclou line number separat
" per colon. Exemple, prem gF sobre qeusta linea i s'obrirà el file: ~/.bashrc:2

" Vim tabs
" Show file name in Tab without path
" set tabline=

" Plugin buftabs shows the names of the buffers
" This setting shows the file name without path
" No ho faig servir mai.
" :let g:buftabs_only_basename=1
" :let g:buftabs_in_statusline=1

" Doc Project pluging  doc
" Obro el vim i després faig Project i el nom del meu file
" Project plugin per veure els files al costat sempre
:let g:proj_window_width=40

"vimrc doc vim spellcheck
"-----
    ":set spell spelllang=en_us
    ":set nospell

" DOC show registers
":registers

" Some cscope shortcuts take from cscope_maps.vim
" wget -O ~/.vim/bundle/cscope_maps/plugin/cscope_maps.vim http://cscope.sourceforge.net/cscope_maps.vim 
" Exemple bo de com utilitzar expand!!! .El truco és posar <c-r>= al principi i
" <c-r> al final!!

" Find reference
"nnoremap <C-f> :cs find s <c-r>=expand("<cword>")<cr><cr>
nnoremap <C-f> :cs find s <c-r>=expand("<cword>")<cr>

" Find definition. Conflicts with see file name. Declaration
nnoremap <C-a> :cs find g <c-r>=expand("<cword>")<cr><cr>

" Open search results in cwindow copen quickfix window
" vim doc cscope doc:
" Type :help cscopequickfix
" Then go navigate to link with Control ]
" La opció a- no funciona en alguns vims i per aix la trec
" No m'ho agava en neovim v0.9.0
"set cscopequickfix=s-,c-,d-,i-,t-,e-,a-
"set cscopequickfix=s-,c-,d-,i-,t-,e-


" Folding wrapping
" jo how-to {
"     set foldmethod=syntax
"  zA		   open OR closed ONE fold recursively
"  zM		   close ALL folds recursively
"  zR		   open ALL folds recursively
"
" }
" set foldmethod=syntax

" Tagbar plugin to see list of functions
nmap <F8> :TagbarToggle<CR>

" Quan tanques un split com per exemple el split que conté
" La llista de funcions passa que el Vim redimensiona la
" resta de split per a que siguin del matiex tamany. En
" canvi jo vull que quan tanco un split la resta és quedi
" igual!!!!! En altres paraules que els splits not
" equal always!!!!
set noequalalways

" Neovim cursor
" https://github.com/neovim/neovim/wiki/FAQ#how-can-i-change-the-cursor-shape-in-the-terminal
":set termguicolors
":hi Cursor guifg=green guibg=green
":hi Cursor2 guifg=red guibg=red
":set guicursor=n-v-c:block-Cursor/lCursor,i-ci-ve:ver25-Cursor2/lCursor2,r-cr:hor20,o:hor50

" Begin Git Gutter Doc Plugin
    " https://github.com/airblade/vim-gitgutter

    " Activate/Deactivate
    " :GitGutterToggle

    " Activate/Deactivate per buffer
    " :GitGutterBufferEnable

    " Toggle highlight
    " GitGutterLineHighlightsToggle

    " Jump to next hunk (change): ]c
    " Jump to previous hunk (change): [c.

    " Open vimdiff
    " GitGutterDiffOrig
" End Git Gutter Doc

" Begin highlight selected text
    hi Visual cterm=none ctermbg=LightYellow ctermfg=cyan
" End highlight selected text

" When doin :set wrap to wrap lines break the lines at the end of
" the word and do not break words
set linebreak

" Python jump to things using indentwise plugin
" python jump/Go to End of block
" Els jumps en visual mode no funcionen :(
nnoremap [e <Plug>(IndentWiseNextLesserIndent)$<Plug>(IndentWisePreviousGreaterIndent)
xnoremap [e <Plug>(IndentWiseNextLesserIndent)$<Plug>(IndentWisePreviousGreaterIndent)
" python jump/go to Beginning of block
nnoremap [b <Plug>(IndentWisePreviousLesserIndent)
xnoremap [b <Plug>(IndentWisePreviousLesserIndent)

" Remove all trailing whitespace by pressing F5
" Source: https://vi.stackexchange.com/questions/454/whats-the-simplest-way-to-strip-trailing-whitespace-from-all-lines-in-a-file
nnoremap <F7> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

