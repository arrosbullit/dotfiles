

; Script executat per Autohotkeys per configurar el teclat 
; bé molt bé  com cal
; Copia aquest fitxer a: 
; %APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.

; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.

SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Això és per evitar aplicar aquest script quan sóc al VirtualBox
#IfWinNotActive ahk_exe VirtualBoxVM.exe

CapsOn=false

*Capslock::
;suspend to prevent calling esc
Suspend on
Send, {ESC}
Suspend off
return

Esc::
;use global variable to keep track of state
if CapsOn = false
{
 CapsOn = true
 SetCapsLockState, on
}
else
{
 CapsOn = false
 SetCapsLockState, off
}
return

; Reemplaca enye per slash
SC027::/
return

; Numbers!!!!
; Explicació: <^>!a vol dir "AltGr a"
; "Suspend on" és pq sense "Suspend on" 
; AltGr es manté premut mentre prems
; la segona tecla, un 1 en aquest cas i
; enlloc d'escriure un 1, escriu whatever
; hi ha a "AltGr 1"
<^>!a::
Send 1
return

<^>!s::
Send 2
return

<^>!d::
Send 3
return

<^>!f::
Send 4
return

<^>!g::
Send 5
return

<^>!h::
Send 6
return

<^>!j::
Send 7
return

<^>!k::
Send 8
return

<^>!l::
Send 9
return

<^>!SC027::
Send 0
return


; Remapejo el asterisc a la fila central o fila 0
SC02B::
Send *
return

; Remapejo la fila 1 (la fila 0 és la central, la 1 la de sobre i la -1 la de sota)
; Mapejo als simbols que venen de la fila 2
; Aqui mapejo tots els AltGr que van a la fila per sobre de la central
<^>!q::
Send {Raw}!
return

<^>!w::
Send "
return

<^>!e::
Send {Raw}#
return

<^>!r::
Send {Raw}$
return

<^>!t::
Send `%
return

<^>!y::
Send &
return

<^>!i::
Send (
return

<^>!o::
Send )
return

<^>!p::
Send {Raw}=
return


; Remapejo la fila -1 (la fila 0 és la central i la -1 la de sota)
; Mapejo \ | [] {} on em convé terni-los


<^>!<::
Send {Raw}\
return

<^>!z::
Send {Raw}\
return

<^>!x::
Send {Raw}|
return


<^>!c::
Send {Raw}{
return

<^>!v::
Send {Raw}}
return

<^>!n::
Send [
return

<^>!m::
Send ]
return



