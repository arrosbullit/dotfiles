

; Script executat per Autohotkeys per configurar el teclat 
; bé molt bé  com cal
; Copia aquest fitxer a: 
; %APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.

; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.

SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Això és per evitar aplicar aquest script quan sóc al VirtualBox
;#IfWinNotActive ahk_exe VirtualBoxVM.exe and #IfWinNotActive ahk_exe ubuntu.exe
;#IfWinNotActive ahk_exe VirtualBoxVM.exe and #IfWinNotActive ahk_exe ubuntu.exe and #IfWinNotActive ahk_exe Xmin.exe
#IfWinNotActive ahk_exe VirtualBoxVM.exe

{

CapsOn=false

Capslock::
;suspend to prevent calling esc
Suspend on
Send, {ESC}
Suspend off
return

Esc::
;use global variable to keep track of state
; Desactivo ESC pq és massa a vegades es posa Capslock i 
; em torno boig
;if CapsOn = false
;{
; CapsOn = true
; SetCapsLockState, on
;}
;else
;{
; CapsOn = false
; SetCapsLockState, off
;}
return

; Reemplaca enye per slash
SC027::/
return

; TODO la slash ha pres el lloc de la enye minúscula
; i com que sé que el lloc que ocupa la enye majúscula
; es pot alliberar pq la enye majúscula no s'utilitza mai
; ergo faig servir el lloc de la enye majúscula per posar-hi
; la enye minúscula


; Desactivo el PageDown pq no paro de prémer-lo sense voler
; Ho torno a activar pq si premo PageUp per error després needo
; PageDown per tornar. Per cert, que PageUp el necessito pq
; una combinació de tecles que fa té el PageUp la faig servir
; en Tmux per copiar coses de la consola
; SC151::
; return

; Numbers!!!!
; Explicació: <^>!a vol dir "AltGr a"
; "Suspend on" és pq sense "Suspend on" 
; AltGr es manté premut mentre prems
; la segona tecla, un 1 en aquest cas i
; enlloc d'escriure un 1, escriu whatever
; hi ha a "AltGr 1"... No, crec que és per
; a evitar bulces infinits quan fas un Send
; de una tecla i no vols aplicar cap hotkeys
; a aquesta tecla
<^>!a::
Suspend on
Send 1
Suspend off
return

<^>!s::
Suspend on
Send 2
Suspend off
return

<^>!d::
Suspend on
Send 3
Suspend off
return

<^>!f::
Suspend on
Send 4
Suspend off
return

<^>!g::
Suspend on
Send 5
Suspend off
return

<^>!h::
Suspend on
Send 6
Suspend off
return

<^>!j::
Suspend on
Send 7
Suspend off
return

<^>!k::
Suspend on
Send 8
Suspend off
return

<^>!l::
Suspend on
Send 9
Suspend off
return

<^>!SC027::
Suspend on
Send 0
Suspend off
return

; Remapejo el asterisc a la fila central o fila 0
SC02B::
Send *
return

; TODO l'asteric ha pres el lloc de la c trencada minúscula
; i com que sé que el lloc que ocupa la c trencada majúscula
; es pot alliberar pq la c trencada majúscula no s'utilitza mai
; ergo faig servir el lloc de la c trencada majúscula per posar-hi
; la c trencada minúscula


; Remapejo la fila 1 (la fila 0 és la central, la 1 la de sobre i la -1 la de sota)
; Mapejo als simbols que venen de la fila 2
; Aqui mapejo tots els AltGr que van a la fila per sobre de la central
<^>!q::
Suspend on
Send {Raw}!
Suspend off
return

<^>!w::
Suspend on
Send "
Suspend off
return

<^>!e::
Suspend on
Send {Raw}#
Suspend off
return

<^>!r::
Suspend on
Send {Raw}$
Suspend off
return

<^>!t::
Suspend on
Send `%
Suspend off
return

<^>!y::
Suspend on
Send &
Suspend off
return

<^>!i::
Suspend on
Send (
Suspend off
return

<^>!o::
Suspend on
Send )
Suspend off
return

<^>!p::
Suspend on
Send {Raw}=
Suspend off
return


; Remapejo la fila -1 (la fila 0 és la central i la -1 la de sota)
; Mapejo \ | [] {} on em convé terni-los


<^>!<::
Suspend on
Send {Raw}\
Suspend off
return

<^>!z::
Suspend on
Send {Raw}\
Suspend off
return

<^>!x::
Suspend on
Send {Raw}|
Suspend off
return

<^>!c::
Suspend on
Send {Raw}{
Suspend off
return

<^>!v::
Suspend on
Send {Raw}}
Suspend off
return

<^>!n::
Suspend on
Send {Raw}[
Suspend off
return

<^>!m::
Suspend on
Send {Raw}]
Suspend off
return

; Finalement la ny minúscula i la c trencada minúscules
; les haig de posar en algun lloc

}


