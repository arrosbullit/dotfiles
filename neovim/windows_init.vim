
" C:\Users\rllu\AppData\Local\nvim\init.vim
" To reload :source C:\Users\rllu\AppData\Local\nvim\init.vim


" Requreix que el vim ho suporti tb. Verue secció "clipboard in vim"!!
" https://stackoverflow.com/questions/11489428/how-to-make-vim-paste-from-and-copy-to-systems-clipboard
" To make all copy paste uses system clipboard
" I think 
" Ojo! quan canvies això tb has de canviar el TextYankPost * o TextYankPost +
" una mica més avall!!!!
"set clipboard=unnamed
set clipboard=unnamedplus

" prevent avoid disable yanking when deleting
nnoremap d "_d
vnoremap d "_d
"vnoremap p "_dP
" prevent avoid disable yanking when changing replacing
nnoremap c "_c
vnoremap c "_c
" prevent avoid disable yanking when deleting
nnoremap x "_x
"vnoremap x "_x

" Highlight la searched word
:set hlsearch
" REC :noh to remove highlihth after search
" Incremental search disabled pq el nvim el té enabled per default
:set incsearch!

" https://neovim.io/doc/user/usr_27.html
" Es case insensitive a no ser que posis alguna majúscula
:set ignorecase smartcase

" Oepn line and go back to normal mode. Do not go to insert mode after
" open line
noremap o o<ESC>
noremap O O<ESC>

" C programming disable automatic comment add
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

