#!/bin/bash

#Installation for soemething related to tmux copy paste install step
#-----
#    sudo apt-get install xclip
#    tmux version >= 1.8
#    bind -t vi-copy y copy-pipe "xclip -sel clip -i"


sudo apt-get install xclip

# bc program is like number parser used by my script to
# identify the correct tmux version.

sudo apt-get install bc
