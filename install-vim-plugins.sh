sudo apt install git

# Install vim
#sudo apt-get install vim-gtk

# Install vim copy paste thing
sudo apt-get install xsel

# Install curl
sudo apt-get install curl

# Install Pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle
curl -LSso ~/.vim/autoload/pathogen.vim http://tpo.pe/pathogen.vim

# Install Control p
mkdir -p ~/.vim/bundle/contrlp.vim
cd ~/.vim
git clone https://github.com/kien/ctrlp.vim.git bundle/ctrlp.vim


# Install RainbowLevels
# cp -a ./vim/dotvim/bundle/rainbow_levels.vim $HOME/.vim/bundle/
#cd ~/.vim/bundle/
#git clone https://github.com/thiagoalessio/rainbow_levels.vim.git

# Modify colors a bit
# These colors are used by syntax highlihthin
# These colors are used by plughin RainbowLevels
sudo cp ./vim/syncolor.vim /usr/share/vim/vim81/syntax/

cd $HOME/.vim/bundle
git clone https://github.com/christoomey/vim-tmux-navigator.git

# Autocomplete  plugin install step. Status: buscant alternativa
# El problema d'aquest autocomplete és que sovint posa la CPU al 100%
# -> n'haig de provar un altre
# https://tabnine.com/
#cd $HOME/.vim/bundle
#git clone https://github.com/zxqfl/tabnine-vim
#

# Autocomplete  plugin install step
# Installation instructions tretes de
cd $HOME/.vim/bundle
curl --fail -L https://github.com/neoclide/coc.nvim/archive/release.tar.gz|tar xzfv -

# Install vim-plug plugin manager needed by coc
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Install plugin to decript encript AES files
#cd ~/.vim/bundle/
#git clone https://github.com/stuartcarnie/vim-openssl.git
# Use my modified version of openssl
mkdir -p ~/.vim/bundle/vim-openssl/plugin
ln -s ~/.dotfiles/vim/dotvim/bundle/vim-openssl/plugin/openssl.vim    ~/.vim/bundle/vim-openssl/plugin/openssl.vim


# Autocomplete  plugin install step
# Installation instructions tretes de
#

#Pre-requisit del coc	
    # Install vim-plug plugin manager needed by coc
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

    cd $HOME/.vim/bundle
    curl --fail -L https://github.com/neoclide/coc.nvim/archive/release.tar.gz|tar xzfv -

        #https://github.com/neoclide/coc.nvim

        #Install fucking yarn
        #----
        #    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
        #    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

        #    On Ubuntu 16.04 or below and Debian Stable, you will also need to configure the NodeSource repository to get a new enough version of Node.js.

        #    Then you can simply:

        #    sudo apt update && sudo apt install yarn

# Per veure la llista de open buffers en tot moment.
# Abandonat pq no sé com configurar-lo
# git clone https://github.com/bling/vim-bufferline ~/.vim/bundle/vim-bufferline

# Install buftabs que tinc copiat
# No ho he fet servir mai...
# cp -a ~/.dotfiles/vim/dotvim/bundle/buftabs ~/.vim/bundle/

# Project plugin. Install process not teste
# https://www.vim.org/scripts/script.php?script_id=69
# Conté la customització de nowrap -> wrap!
# cp -a ~/.dotfiles/vim/dotvim/bundle/vim-project-plugin ~/.vim/bundle/
mkdir -p ~/.vim/bundle/vim-project-plugin/plugin
ln -s ~/.dotfiles/vim/dotvim/bundle/vim-project-plugin/plugin/project.vim    ~/.vim/bundle/vim-project-plugin/plugin/project.vim


# Install tabular per fer taules
mkdir -p ~/.vim/bundle/
cd ~/.vim/bundle
git clone  https://github.com/godlygeek/tabular.git

# Install tagbar to see list of functions in file given a ttags file
mkdir -p ~/.vim/bundle/
cd ~/.vim/bundle
git clone https://github.com/preservim/tagbar.git


# Vim git show differences
mkdir -p ~/.vim/bundle/
cd ~/.vim/bundle
git clone https://github.com/airblade/vim-gitgutter.git

# cscope no cal
# mkdir -p ~/.vim/bundle/cscope_maps/plugin
# wget -O ~/.vim/bundle/cscope_maps/plugin/cscope_maps.vim http://cscope.sourceforge.net/cscope_maps.vim

# Install Python indentwise to jump to end of blocks 
cp -a ~/.dotfiles/vim/dotvim/bundle/vim-indentwise ~/.vim/bundle/

